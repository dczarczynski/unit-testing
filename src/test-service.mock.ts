import { Promise } from "ts-promise";
import { TestService } from "./test-service.interface";

export const testServiceMock: TestService = {
  fetchDataFromApi() {
    return Promise.resolve(['1', '2', '3']);
  }
}