import { TestService } from "./test-service.interface";

export const testService: TestService = {
  fetchDataFromApi(): Promise<string[]> {
    return fetch('http://some-api.url')
      .then(value => value.json());
  }
}