import 'mocha';
import { expect } from 'chai';
import { transformString } from './string-2';

describe('String transform function', () => {
  it('should transform first letter of single word string', () => {
    const string = 'test';

    const result = transformString(string);

    expect(result).to.equal('Test');
  });

  it('should transform first letter of multiple words string', () => {
    const string = 'test test test';

    const result = transformString(string);

    expect(result).to.equal('Test Test Test');
  });

  it('should return string with only first letter of each word capitalized', () => {
    const string = 'teSt';

    const result = transformString(string);

    expect(result).to.equal('Test');
  });

  it('should return error message when empty string is passed', () => {
    const string = '';

    const result = transformString(string);

    expect(result).to.equal('Empty string was passed');
  });

  it('should return error message when undefined is passed', () => {
    const string = undefined;

    const result = transformString(string);

    expect(result).to.equal('Empty string was passed');
  });

  it('should return error message when null is passed', () => {
    const string = null;

    const result = transformString(string);

    expect(result).to.equal('Empty string was passed');
  });
});