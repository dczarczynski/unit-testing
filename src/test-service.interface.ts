export interface TestService {
  fetchDataFromApi(): Promise<string[]>
}