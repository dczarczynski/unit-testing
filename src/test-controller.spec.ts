import 'mocha';
import * as chai from 'chai';
import * as spies from 'chai-spies';
import Promise from 'ts-promise';

chai.use(spies);
const expect = chai.expect;

import { TestController } from './test-controller';
import { testServiceMock } from './test-service.mock';

describe.only('TestController', () => {
  let controller: TestController;

  beforeEach(() => {
    controller = new TestController(testServiceMock);
  });

  it('should properly fetch data on init', (done) => {
    controller.onInit()
      .then(() => {
        expect(controller.data).to.have.lengthOf(3);
        expect(controller.data[0]).to.equal('1');
        done();
      });
  });

  it('should add data item to empty data array', () => {
    const item = 'some test item';
    controller.data = [];

    controller.addItemToData(item);

    expect(controller.data).to.have.lengthOf(1);
    expect(controller.data[0]).to.equal('some test item');
  });

  it('should add data item to existing data array', () => {
    const item = 'some test item';
    controller.data = ['test1', 'test2'];

    controller.addItemToData(item);

    expect(controller.data).to.have.lengthOf(3);
    expect(controller.data[2]).to.equal('some test item');
  });

  it('should catch error message', () => {
    chai.spy.on(testServiceMock, 'fetchDataFromApi',
      () => Promise.reject(new Error('error!'))
    );

    controller.onInit()
      .then(() => expect(controller.error).to.equal('error!'));
  });

});
