export const transformString = (string: string): string => {
  if (!string) {
    return 'Empty string was passed';
  }

  return string
    .split(' ')
    .map(
      word => word
        .split('')
        .map((letter, index) => index === 0 ? letter.toUpperCase() : letter.toLowerCase())
        .join('')
    )
    .join(' ');
}