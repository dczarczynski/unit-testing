import 'mocha';
import { expect } from 'chai';

import { addString } from './index';

describe('String concat function', () => {

  it('should concat string with a number', () => {
    const testString = 'test string';

    const result = addString(testString);

    expect(result).to.equal('test string2');
  });

  it('should concat number with a number', () => {
    const testString = 2;

    const result = addString(testString);

    expect(result).to.equal('22');
  });

});