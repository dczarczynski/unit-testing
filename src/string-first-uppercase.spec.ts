import 'mocha';
import { expect } from 'chai';

import { firstUppercase } from './string-first-uppercase';

describe('String first uppercase function', () => {

  it('should uppercase first letter of single word string', () => {
    const testString = 'test';

    const result = firstUppercase(testString);

    expect(result).to.equal('Test');
  });

  it('should uppercase first letter of double word string', () => {
    const testString = 'test test';

    const result = firstUppercase(testString);

    expect(result).to.equal('Test Test');
  });

  it('should uppercase first letter of multiple word string', () => {
    const testString = 'test test blabla array test string';

    const result = firstUppercase(testString);

    expect(result).to.equal('Test Test Blabla Array Test String');
  });

  it('should uppercase only first letter of caps string', () => {
    const testString = 'TEST';

    const result = firstUppercase(testString);

    expect(result).to.equal('Test');
  });

  it('should uppercase only first letter of caps string', () => {
    const testString = 'tEST';

    const result = firstUppercase(testString);

    expect(result).to.equal('Test');
  });

  it('should return undefined when empty string is passed', () => {
    const testString = '';

    const result = firstUppercase(testString);

    expect(result).to.equal(undefined);
  });

  it('should return undefined when undefined is passed', () => {
    const testString = undefined;

    const result = firstUppercase(testString);

    expect(result).to.equal(undefined);
  });

  it('should return undefined when null is passed', () => {
    const testString = '';

    const result = firstUppercase(testString);

    expect(result).to.equal(undefined);
  });

});