export const firstUppercase = (string: string): string =>
  string
    ? string
      .split(' ')
      .map(word =>
        word
          .split('')
          .map((letter, index) => index ? letter.toLowerCase() : letter.toUpperCase())
          .join('')
      )
      .join(' ')
    : undefined;