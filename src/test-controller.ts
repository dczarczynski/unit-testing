import { TestService } from "./test-service.interface";

export class TestController {
  data: string[];
  error: string;

  constructor(private testService: TestService) {
    this.data = [];
  }

  onInit() {
    return this.testService.fetchDataFromApi()
      .then(value => this.data = value)
      .catch(error => this.error = error.message);
  }

  addItemToData(item) {
    this.data = [...this.data, item];
  }
}